package com.yun.main.tag.core;

import java.io.IOException;
import java.io.Writer;
import java.util.Map;

import com.yun.main.interceptor.TokenValidator;
import com.yun.main.utils.HttpFormTokenAnalyzer;
import com.yun.main.utils.SpringContext;

import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;

/**
 * 
 * <p>生成防止表单重复提交的表单隐藏域input的freemark标签 </p>
 * @Package com.dzq.web.freemarker.tags 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-4-18 上午11:17:17 
 * @version V1.0
 */
public class TokenTagFreemarkerDirective implements TemplateDirectiveModel {
	
	public TokenTagFreemarkerDirective(TokenValidator tokenValidator){
		if(tokenValidator==null){
			throw new RuntimeException("构造TokenTagFreemarkerDirective失败，TokenValidator不能为null");
		}
		this.tokenValidator = tokenValidator;
		httpFormTokenanalyzer = new HttpFormTokenAnalyzer(this.tokenValidator);
	}
	  
	private  TokenValidator tokenValidator;
	private HttpFormTokenAnalyzer httpFormTokenanalyzer;
    public void execute(Environment env, Map params, TemplateModel[] loopVars,  
            TemplateDirectiveBody body) throws TemplateException, IOException {  
        // 检查是否传递参数，此指令禁止传参！  
        if (!params.isEmpty()) {  
            throw new TemplateModelException(  
                    "This directive doesn't allow parameters.");  
        }  
        // 禁用循环变量  
        /* 
         * 循环变量 
                 用户定义指令可以有循环变量，通常用于重复嵌套内容，基本用法是：作为nested指令的参数传递循环变量的实际值，而在调用用户定义指令时，在${"<@…>"}开始标记的参数后面指定循环变量的名字 
                 例子： 
            <#macro repeat count> 
              <#list 1..count as x> 
                <#nested x, x/2, x==count> 
              </#list> 
            </#macro> 
            <@repeat count=4 ; c, halfc, last> 
              ${c}. ${halfc}<#if last> Last!</#if> 
            </@repeat>  
        */  
        if (loopVars.length != 0) {  
            throw new TemplateModelException(  
                    "This directive doesn't allow loop variables.");  
        }  
  
        // 指令内容体不为空  
        if (body != null) {  
            // Executes the nested body. Same as <#nested> in FTL, except  
            // that we use our own writer instead of the current output writer.  
            body.render(new TokenWriter(env.getOut()));  
        } else { 
        	body = new TemplateDirectiveBody() {
				
				@Override
				public void render(Writer out) throws TemplateException, IOException {
					out.write(generateToken());  
				}
			};
			body.render(env.getOut());
//            throw new RuntimeException("missing body");  
			
        }  
    }  
    
	private String generateToken() {
		if(tokenValidator==null){
			tokenValidator =  (TokenValidator) SpringContext.getBean(TokenValidator.class);
			httpFormTokenanalyzer = new HttpFormTokenAnalyzer(this.tokenValidator);
		}
		/*StringBuilder sb = new StringBuilder();
		tokenHelper = (TokenHelper) SpringContext.getBean(TokenHelper.class);
		String[] tokenArgs = tokenHelper.generateToken();
	
		//sb.append("<input type='hidden' name='" + tokenHelper.TOKEN_NAME_FIELD + "FormId' value='" + this.getFormId() + "'/>");
		sb.append("<input type='hidden' name='" + tokenHelper.TOKEN_NAME_FIELD + "' value='" + tokenArgs[0] + "'/>");
		sb.append("<input type='hidden' name='" + tokenArgs[0] + "' value='" + tokenArgs[1] + "'/>");
*/
		return httpFormTokenanalyzer.getFormInputHtml();
	}
  
    /** 
     * 表单防重复提交隐藏域输出流的包装器(输出表单隐藏域) 
     */  
    private  class TokenWriter extends Writer {  
  
        private final Writer out;  
  
        TokenWriter(Writer out) {  
            this.out = out;  
        }  
  
        public void write(char[] cbuf, int off, int len) throws IOException {  
            out.write(generateToken());  
        }  
  
        public void flush() throws IOException {  
            out.flush();  
        }  
  
        public void close() throws IOException {  
            out.close();  
        }  
    }  
  
}  
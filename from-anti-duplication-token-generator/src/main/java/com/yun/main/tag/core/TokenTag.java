package com.yun.main.tag.core;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import com.yun.main.interceptor.TokenHelper;
import com.yun.main.interceptor.TokenValidator;
import com.yun.main.utils.HttpFormTokenAnalyzer;
import com.yun.main.utils.SpringContext;

/**
 * 
 * <p>生成防止表单重复提交的表单隐藏域input的jsp标签</p>
 * @Package com.yun.main.tag.core 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-4-18 上午10:36:34 
 * @version V1.0
 */
public class TokenTag extends TagSupport {

	private static final long serialVersionUID = 1L;
	private TokenValidator tokenValidator;
	private HttpFormTokenAnalyzer httpFormTokenanalyzer;
	
	private String formId;
	
	/**
	 * @return the formId
	 */
	public String getFormId() {
		return formId;
	}

	/**
	 * @param formId the formId to set
	 */
	public void setFormId(String formId) {
		this.formId = formId;
	}

	private String generateToken() {
		if(tokenValidator==null){
			tokenValidator = (TokenHelper) SpringContext.getBean(TokenHelper.class);
			httpFormTokenanalyzer = new HttpFormTokenAnalyzer(this.tokenValidator);
		}
		/*StringBuilder sb = new StringBuilder();
		tokenHelper = (TokenHelper) SpringContext.getBean(TokenHelper.class);
		String[] tokenArgs = tokenHelper.generateToken();
	
		//sb.append("<input type='hidden' name='" + tokenHelper.TOKEN_NAME_FIELD + "FormId' value='" + this.getFormId() + "'/>");
		sb.append("<input type='hidden' name='" + tokenHelper.TOKEN_NAME_FIELD + "' value='" + tokenArgs[0] + "'/>");
		sb.append("<input type='hidden' name='" + tokenArgs[0] + "' value='" + tokenArgs[1] + "'/>");
*/
		return httpFormTokenanalyzer.getFormInputHtml();
	}

	@Override
	public int doEndTag() throws JspException {
		try {
			JspWriter out = this.pageContext.getOut();
			String naviHtml = generateToken();
			out.append(naviHtml);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return super.doEndTag();
	}

	public TokenValidator getTokenHelper() {
		return tokenValidator;
	}

	public void setTokenHelper(TokenValidator tokenHelper) {
		this.tokenValidator = tokenHelper;
	}

	
}

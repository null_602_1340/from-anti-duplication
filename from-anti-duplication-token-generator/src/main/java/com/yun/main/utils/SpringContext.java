package com.yun.main.utils;

import javax.servlet.http.HttpServletRequest;

import org.springframework.context.ApplicationContext;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * @Discription 根据spring上下文获取该上下文对象实例
 * @Email zhenchuan.wang@heysroad.com
 * @Author 王振川,修改者：黄乡南
 * @Date 2014年10月08日
 */
public class SpringContext {

	private static Object spring_init_lock = new Object();
	private static ApplicationContext springContext = null;

	public static void setSpringContext(ApplicationContext springContext) {
		SpringContext.springContext = springContext;
	}

	private static ApplicationContext getSpringContext() {
		if (springContext != null)
			return springContext;

		synchronized (spring_init_lock) {
			if (springContext == null) {
				springContext = WebApplicationContextUtils
						.getRequiredWebApplicationContext(getRequest()
								.getServletContext());
			}
		}
		return springContext;
	}

	public static Object getBean(String beanName) {
		ApplicationContext ctx = getSpringContext();
		Object obj = ctx.getBean(beanName);
		return obj;
	}

	public static Object getBean(Class<?> clazz) {
		ApplicationContext ctx = getSpringContext();
		Object obj = ctx.getBean(clazz);
		return obj;
	}

	public static <T> T getBean(String beanName, Class<T> clazz) {
		ApplicationContext ctx = getSpringContext();
		T obj = ctx.getBean(beanName, clazz);
		return obj;
	}

	public static HttpServletRequest getRequest() {
		return ((ServletRequestAttributes) RequestContextHolder
				.getRequestAttributes()).getRequest();
	}
}

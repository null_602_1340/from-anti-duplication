package com.yun.main.interceptor;

/**
 * 
 * <p>token的仓库 </p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午2:10:30 
 * @version V1.0
 */
public interface TokenStorage {

	/**
	 * 
	 * <p>
	 *  说明：保存表单tonken
	 * </p>
	 * @param namespaceTokenKey
	 * @param formTokenVal
	 * @return
	 * @author 黄乡南
	 */
	public boolean save(FormToken formToken);
	/**
	 * 
	 * <p>
	 *  说明：通过tokenKey获取tokenval
	 * </p>
	 * @param formTokenKey
	 * @return
	 * @author 黄乡南
	 */
	public String get(String formTokenKey);
	
	/**
	 * 
	 * <p>
	 *  说明：通过tokenKey删除tokenval
	 * </p>
	 * @param formTokenKey
	 * @author 黄乡南
	 */
	public void delete(String formTokenKey);

}

package com.yun.main.interceptor;

/**
 * 
 * <p>url拦截策略，用于判断某个url某个请求是否拦截 </p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @email hxiangnan@126.com
 * @date 2016-11-14 下午5:39:40 
 * @version V1.0
 */
public interface TokenInterceptStrategy {
	/**
	 * 
	 * 
	 * 
	 * <p> 
	 *	说明：是否对该url进行表单防重复拦截
	 *	</p>
	 * @return
	 * @author 黄乡南
	 */
	public boolean isIntercept();
}

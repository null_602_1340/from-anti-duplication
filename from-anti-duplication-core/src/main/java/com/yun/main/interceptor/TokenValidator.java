package com.yun.main.interceptor;

/**
 * 
 * <p>Token验证器的接口</p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午2:08:12 
 * @version V1.0
 */
public interface TokenValidator {
	/**
	 * 
	 * <p> 
	 *	说明：生成token的一些配置
	 *	</p>
	 * @return TokenConfig
	 * @author 黄乡南
	 */
	public TokenConfig getTokenConfig();

	/**
	 * 
	 * <p>
	 *  说明：验证tonken是否有效。如果有效则返回true，如果已经使用过了或过期了就表示无效了就返回false,如果传人null只，则返回false
	 * </p>
	 * @param formToken
	 * @return 验证tonken是否有效。如果有效则返回true，如果已经使用过了或过期了就表示无效了就返回false
	 * @author 黄乡南
	 */
	public boolean validToken(FormToken formToken);
	
	/**
	 * 
	 * <p>
	 *  说明：创建一个新的表单token
	 * </p>
	 * @return FormToken 
	 * @author 黄乡南
	 */
	public FormToken generateToken();

}

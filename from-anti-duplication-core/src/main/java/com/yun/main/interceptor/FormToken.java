package com.yun.main.interceptor;

import java.io.Serializable;

/**
 * 
 * <p>表单防重复提交验证token</p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午2:08:12 
 * @version V1.0
 */
public class FormToken implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 每个表单生成一个对应的key，通过这个key可以获取可以对应的val
	 */
	private String tokenKey;
	private String tokenVal;
	/**
	 * 表单token的有效期。单位为毫秒，如1000=1000毫秒=1秒。
	 */
	private Long expires;
	
	public FormToken(String tokenKey, String tokenVal){
		this.tokenKey = tokenKey;
		this.tokenVal = tokenVal;
	}
	
	
	public FormToken(String tokenKey, String tokenVal,Long expires) {
		super();
		this.tokenKey = tokenKey;
		this.tokenVal = tokenVal;
		this.expires = expires;
	}
	public String getTokenKey() {
		return tokenKey;
	}
	public void setTokenKey(String tokenKey) {
		this.tokenKey = tokenKey;
	}
	public String getTokenVal() {
		return tokenVal;
	}
	public void setTokenVal(String tokenVal) {
		this.tokenVal = tokenVal;
	}
	
	
	

	public Long getExpires() {
		return expires;
	}
	public void setExpires(Long expires) {
		this.expires = expires;
	}


	@Override
	public String toString() {
		return "FormToken [tokenKey=" + tokenKey + ", tokenVal=" + tokenVal
				+ ", expires=" + expires + "]";
	}
}

package com.yun.main.interceptor;

/**
 * 
 * <p>token配置信息</p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午2:08:12 
 * @version V1.0
 */
public class TokenConfig {
	
	/** 
	 * 前端表单token文本框的name的默认值。默认为form.token.name。
	 * 该name对应的val值对应tokenkey；
	 */  
	public  final String TOKEN_NAME_FIELD = "form.token.name";
	/** 
	 * token存储器用于存储key-val时，对key加前缀的前缀默认值，用于限定key的范围。
	 */  
	public  final String TOKEN_NAMESPACE = "form.tokens";
	/**
	 * 表单token的有效期。单位为毫秒，如1000=1000毫秒=1秒。
	 * 默认：600000毫秒=10分钟
	 */
	public  final Long TOKEN_CONTINUE_TIME = 600000L;  
	
	/**
	 * 
	 * <p>
	 *  说明：前端表单token文本框的name值。默认为form.token.name。该name对应的val值对应tokenkey
	 * </p>
	 * @return 默认返回form.token.name
	 * @author 黄乡南
	 */
	public String getTokenInputName(){
		return TOKEN_NAME_FIELD;
	}
	
	/**
	 * 
	 * <p>
	 *  说明：token存储器用于存储key-val时，对key加前缀，用于限定key的范围。前缀默认值：form.tokens
	 * </p>
	 * @return
	 * @author 黄乡南
	 */
	public String getTokenNamespace(){
		return TOKEN_NAMESPACE;
	}
	
	/**
	 * 
	 * <p>
	 *  说明：表单token的有效期。单位为毫秒，如1000=1000毫秒=1秒。
	 * 默认：600000毫秒=10分钟
	 * </p>
	 * @return 表单token的有效期。单位为毫秒，如1000=1000毫秒=1秒。
	 * @author 黄乡南
	 */
	public Long getTokenExpires(){
		return TOKEN_CONTINUE_TIME;
	}
	
}

package com.yun.main.interceptor;

import java.math.BigInteger;
import java.util.Random;
import java.util.concurrent.locks.Lock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <p>表单拦截验证器实现类</p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午4:44:50 
 * @version V1.0
 */
public class TokenHelper implements TokenValidator {
	private static Logger logger = LoggerFactory.getLogger(TokenHelper.class);
	
	private final TokenLock tokenLock;
	private final Random RANDOM = new Random();
//	protected StringRedisTemplate redisTemplate;
	
	private TokenStorage tokenStorage;

	public TokenHelper(TokenStorage tokenStorage) {
		super();
		this.tokenStorage = tokenStorage;
		tokenLock = new TokenLock();
	}


	/*public String[] generateToken() {
		String tokenName = generateGUID();
		String token = generateGUID();
		String tokenName0 = buildTokenSessionAttributeName(tokenName);
		ValueOperations<String, String> vo = redisTemplate.opsForValue();
		vo.set(tokenName0, token, 1, TimeUnit.MINUTES);
		return new String[] { tokenName, token };
	}*/
	public FormToken generateToken() {
		String tokenkey = generateGUID();
		String tokenVal = generateGUID();
		String namespaceTokenKey = buildTokenSessionAttributeName(tokenkey);//带命名空间的tokenKey
		
//		FormToken formToken  = new FormToken(tokenkey,tokenVal,getTokenConfig().getTokenExpires());
		FormToken namespaceFormToken  = new FormToken(namespaceTokenKey,tokenVal,getTokenConfig().getTokenExpires());
		
		if(tokenStorage.save(namespaceFormToken)){
			return namespaceFormToken;
		}
		return null;
	}

	

	/**
	 * 构建一个基于token名字的带有命名空间为前缀的token名字
	 * 
	 * @param tokenName
	 * @return token命名空间加上tokenName
	 */
	public String buildTokenSessionAttributeName(String tokenName) {
		return getTokenConfig().getTokenNamespace() + "." + tokenName;
	}


	/**
	 * 移除Token
	 * 
	 * @param request
	 */
	public void removeToken(FormToken formToken) {
		String tokenName = formToken.getTokenKey();
		tokenStorage.delete(tokenName);
	}

	
	

	public String generateGUID() {
		return new BigInteger(165, RANDOM).toString(36).toUpperCase();
	}

	/**
	 * 
	 * <p>
	 * 说明：生产表单html标签
	 * </p>
	 * 
	 * @return
	 * @auth 黄乡南
	 */
	public String getFormInputHtml() {
		StringBuilder sb = new StringBuilder();
		FormToken tokenArgs = this.generateToken();
		sb.append("<input type='hidden' name='" + this.getTokenConfig().getTokenInputName()
				+ "' value='" + tokenArgs.getTokenKey() + "'/>");
		sb.append("<input type='hidden' name='" + tokenArgs.getTokenKey() + "' value='"
				+ tokenArgs.getTokenVal() + "'/>");
		return sb.toString();
	}

	@Override
	public TokenConfig getTokenConfig() {
		return new TokenConfig();
	}

	@Override
	public boolean validToken(FormToken formToken) {
		if(logger.isDebugEnabled()){
			logger.debug("formToken："+formToken);
		}
		String token = formToken.getTokenVal();
		if (token == null) {
			return false;
		}
		Lock lock = tokenLock.getLock(token);
		try {
			lock.lock();
			if (!doValidToken(formToken)) {
				logger.error("from-interceptor:未通过验证...");
				return false;
			}
			this.removeToken(formToken);
			return true;
		} finally {
			lock.unlock();
			tokenLock.removeLock(token);
		}
	}

	private boolean doValidToken(FormToken formToken) {
		String tokenName = formToken.getTokenKey();
		String token = formToken.getTokenVal();

		if (token == null) {
			return false;
		}

		// String cacheToken = redisCacheClient.listLpop(tokenCacheName);
		String cacheToken =tokenStorage.get(tokenName);
		if (logger.isDebugEnabled()) {
			logger.debug("[token=" + token + ",cacheToken" + cacheToken + "]");
		}
		if (!token.equals(cacheToken)) {
			logger.warn("Session中的Token：" + token + " 缓存中的Token" + cacheToken
					+ "不一致");
			return false;
		}
		return true;
	}

}
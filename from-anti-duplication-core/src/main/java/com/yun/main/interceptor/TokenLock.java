package com.yun.main.interceptor;

import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 
 * <p>线程锁工具 ，表单防止重复提交锁住单个token的删除和查询。</p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午12:06:38 
 * @version V1.0
 */
public class TokenLock{
	private static Logger logger = LoggerFactory.getLogger(TokenLock.class);  
	
	private Map<String,Lock> mapLocks = new HashMap<String, Lock>();
	
	/**
	 * 清理所的间隔时间，单位为秒,最小为1秒，小于1秒会自动设置为1秒。默认600秒
	 */
	private int cleanLockIntervalTime=600;
	
	private boolean isActCleanLockTimer=true;;
	
	
	public TokenLock() {
		super();
		initCleanLockTimer();
	}
	/**
	 * 
	 * @param cleanLockIntervalTime 清理所的间隔时间，单位为秒
	 * @param isActCleanLockTimer 是否激活清理锁的线程
	 */
	public TokenLock(int cleanLockIntervalTime, boolean isActCleanLockTimer) {
		super();
		this.cleanLockIntervalTime = cleanLockIntervalTime<1?1:cleanLockIntervalTime;
		this.isActCleanLockTimer = isActCleanLockTimer;
		initCleanLockTimer();
	}

	public synchronized Lock getLock(String token){
		Lock lock = null;
		lock = mapLocks.get(token);
		if(lock==null){
			lock = new ReentrantLock();
			mapLocks.put(token, lock);
		}
		return lock;
	}
	public synchronized void removeLock(String token){
		mapLocks.remove(token);
	}
	public synchronized void removeAllLock(){
		mapLocks= new HashMap<String, Lock>() ;
	}
	
	/**
	 * 
	 * <p>锁资源清理器</p>
	 * @Package com.yun.main.interceptor 
	 * @author 黄乡南
	 * @e-mail 823798218@qq.com
	 * @date 2016-7-5 下午2:02:27 
	 * @version V1.0
	 */
	public static class TokenLockCleaner extends TimerTask{
		private  TokenLock tokenLock =null;
		
		public TokenLockCleaner(TokenLock tokenLock) {
			super();
			this.tokenLock = tokenLock;
		}

		@Override
		public void run() {
			tokenLock.removeAllLock();
		}
		
	}
	
	/**
	 * 
	 * <p>
	 *  说明：初始化定时器，定时清理锁资源
	 * </p>
	 * @author 黄乡南
	 */
	private void initCleanLockTimer(){
		if(isActCleanLockTimer){
			Timer timer = new Timer();
			timer.schedule(new TokenLockCleaner(this), cleanLockIntervalTime*1000,cleanLockIntervalTime*1000);
		}
	}
}

@rem 请把该批处理放到rpc工程项目下
setlocal
@echo off

echo Using CLASSPATH:       "%CLASSPATH%"
rem 获取当前文件夹目录。
set "CURRENT_DIR=%cd%"
echo Using CURRENT_DIR:       "%CURRENT_DIR%"
rem 设置项目的的home目录
set "project_base_dir=%CURRENT_DIR%"
echo project_base_dir: "%project_base_dir%"

rem 设置dzq-core-platform工程的路径
rem set "dzq-core-platform_dir=%project_base_dir%\dzq-core-platform"
rem echo dzq-core-platform_dir: "%dzq-core-platform_dir%"
call mvn clean
call mvn eclipse:clean


echo clean success !
pause
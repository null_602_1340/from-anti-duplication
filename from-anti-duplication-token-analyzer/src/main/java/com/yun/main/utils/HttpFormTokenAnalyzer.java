package com.yun.main.utils;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.yun.main.interceptor.FormToken;
import com.yun.main.interceptor.TokenConfig;
import com.yun.main.interceptor.TokenValidator;

/**
 * 
 * <p>token验证器工具类</p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午2:08:12 
 * @version V1.0
 */
public class HttpFormTokenAnalyzer {
	private static Logger logger = LoggerFactory
			.getLogger(HttpFormTokenAnalyzer.class);
	
	private TokenValidator tokenValidator;
	
	
	
	
	public HttpFormTokenAnalyzer(TokenValidator tokenValidator) {
		super();
		if(tokenValidator==null){
			throw new RuntimeException("tokenValidator不能为null");
		}
		this.tokenValidator = tokenValidator;
	}


	/**
	 * 
	 * <p>
	 *  说明：从请求对象里获取token信息
	 * </p>
	 * @param request
	 * @param tokenConfig 
	 * @return
	 * @author 黄乡南
	 */
	public static  FormToken getTokenByRequst(HttpServletRequest request,TokenConfig tokenConfig) {
		String tokenKey = request.getParameter(tokenConfig.getTokenInputName());

		if (tokenKey == null) {
			logger.debug("no token name found -> Invalid token");
			return null;
		}

		String tokenVal = getToken(request, tokenKey);
		if (tokenVal == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("找不到token名称" + tokenKey);
			}
			return null;
		}
		FormToken formToken = new FormToken(tokenKey, tokenVal);
		return formToken;
	}
	
	/**
	 * 
	 * <p>
	 *  说明：从request对象中获取指定token
	 * </p>
	 * @param request
	 * @return
	 * @author 黄乡南
	 */
	public  FormToken getTokenByRequst(HttpServletRequest request) {
		String tokenKey = request.getParameter(tokenValidator.getTokenConfig().getTokenInputName());
		if (tokenKey == null) {
			logger.debug("no token name found -> Invalid token");
			return null;
		}
		
		String tokenVal = getToken(request, tokenKey);
		if (tokenVal == null) {
			if (logger.isDebugEnabled()) {
				logger.debug("找不到token名称" + tokenKey);
			}
			return null;
		}
		FormToken formToken = new FormToken(tokenKey, tokenVal);
		return formToken;
	}
	
	
	/**
	 * 从请求域中获取给定token名字的token值
	 * 
	 * @param tokenName
	 */
	public static String getToken(HttpServletRequest request, String tokenName) {
		if (tokenName == null) {
			return null;
		}
		Map<String, String[]> params = request.getParameterMap();
		String[] tokens = (String[]) (String[]) params.get(tokenName);
		String token;
		if ((tokens == null) || (tokens.length < 1)) {
			logger.warn("Could not find token mapped to token name " + tokenName);
			return null;
		}

		token = tokens[0];
		return token;
	}


	public static String getFormInputHtml(FormToken formToken,TokenConfig tokenConfig) {
		StringBuilder sb = new StringBuilder();
		sb.append("<input type='hidden' name='" + tokenConfig.getTokenInputName()
				+ "' value='" + formToken.getTokenKey() + "'/>");
		sb.append("<input type='hidden' name='" + formToken.getTokenKey() + "' value='"
				+ formToken.getTokenVal() + "'/>");
		return sb.toString();
	}
	public  String getFormInputHtml() {
		FormToken formToken = tokenValidator.generateToken();
		StringBuilder sb = new StringBuilder();
		sb.append("<input type='hidden' name='" + tokenValidator.getTokenConfig().getTokenInputName()
				+ "' value='" + formToken.getTokenKey() + "'/>");
		sb.append("<input type='hidden' name='" + formToken.getTokenKey() + "' value='"
				+ formToken.getTokenVal() + "'/>");
		return sb.toString();
	}
	
}

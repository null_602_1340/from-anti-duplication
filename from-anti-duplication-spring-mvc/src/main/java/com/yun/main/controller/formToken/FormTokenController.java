package com.yun.main.controller.formToken;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.yun.main.interceptor.FormToken;
import com.yun.main.interceptor.TokenHelper;



@Controller
@RequestMapping("view/form-token")
public class FormTokenController {

	@Autowired
	private TokenHelper tokenHelper;
	
	@RequestMapping(value="get-token",method=RequestMethod.GET)
	@ResponseBody
	public Map<String, Object> getToken(HttpServletRequest request) {
		FormTokenVO token = new FormTokenVO();
		
		FormToken tokenArgs = tokenHelper.generateToken();

		token.setFormkey(tokenHelper.getTokenConfig().TOKEN_NAME_FIELD);
		token.setFormHidden1(tokenArgs.getTokenKey());
		token.setFormHidden2(tokenArgs.getTokenVal());
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("resultCode", 1000);
		map.put("resultMsg", "生成Token成功");
		map.put("resultObj", token);
		return map;
	}
	
	class FormTokenVO{
		private String formkey;
		private String formHidden1;
		private String formHidden2;
		/**
		 * @return the formkey
		 */
		public String getFormkey() {
			return formkey;
		}
		/**
		 * @param formkey the formkey to set
		 */
		public void setFormkey(String formkey) {
			this.formkey = formkey;
		}
		/**
		 * @return the formHidden1
		 */
		public String getFormHidden1() {
			return formHidden1;
		}
		/**
		 * @param formHidden1 the formHidden1 to set
		 */
		public void setFormHidden1(String formHidden1) {
			this.formHidden1 = formHidden1;
		}
		/**
		 * @return the formHidden2
		 */
		public String getFormHidden2() {
			return formHidden2;
		}
		/**
		 * @param formHidden2 the formHidden2 to set
		 */
		public void setFormHidden2(String formHidden2) {
			this.formHidden2 = formHidden2;
		}
	}
}

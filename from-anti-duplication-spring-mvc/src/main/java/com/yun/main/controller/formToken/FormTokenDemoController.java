package com.yun.main.controller.formToken;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.serializer.MapSerializer;

@Controller
@RequestMapping("formtest")
public class FormTokenDemoController {
	
	@RequestMapping()
	public String formTestInput(){
		return "formtest";
	}
	@RequestMapping("ajax-sub-from")
	@ResponseBody
	public Map<String, Object> ajaxSubForm(){
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("resultCode", 1000);
		map.put("resultMsg", "ajxa成功");
		map.put("resultObj", "成功");
		return map;
	}
	@RequestMapping("sub-from")
	public String subForm(Model model){
		model.addAttribute("msg", "成功");
		return "formtest-jsp";
	}
	
	
	@RequestMapping("jsp")
	public String jspForm(){
		return "formtest-jsp";
	}

}

package com.yun.main.interceptor;

import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.yun.main.utils.HttpFormTokenAnalyzer;

/**
 * 
 * @Discription 表单重复提交拦截器
 * @Email  zhenchuan.wang@heysroad.com
 * @Project mall-buy-pc
 * @Author  wangzhenchuan
 * @Date  2015年1月13日
 */
public class TokenInterceptor implements HandlerInterceptor {
	private static Logger logger = LoggerFactory.getLogger(TokenInterceptor.class);  


	private TokenValidator tokenValidator;
	
	private TokenInterceptStrategy tokenInterceptStrategy;
	
	private HttpFormTokenAnalyzer httpFormTokenAnalyzer;
	
	
	
	public TokenInterceptor(TokenValidator tokenValidator) {
		super();
		this.tokenValidator = tokenValidator;
		httpFormTokenAnalyzer = new HttpFormTokenAnalyzer(this.tokenValidator);
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object obj, Exception err)
					throws Exception {
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object obj, ModelAndView mav)
					throws Exception {
	}

	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		if (handler instanceof HandlerMethod) {
			//是否需要校验表单重复提交
			if(request.getParameter(tokenValidator.getTokenConfig().getTokenInputName())!=null){				
				return handleToken(request, response, handler);  		
			}
		} 
		  
		return true;
	}
	
	protected boolean handleToken(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		FormToken formToken =httpFormTokenAnalyzer.getTokenByRequst(request);
		if( !tokenValidator.validToken(formToken)){
			return handleInvalidToken(request, response, handler);
		}else{
			return true;
		}
	}

	/** 
	 * 当出现一个非法令牌时调用 
	 */  
	protected boolean handleInvalidToken(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception  
	{  
		response.setCharacterEncoding("UTF-8");  
		response.setContentType("application/json; charset=utf-8");  
		PrintWriter out = response.getWriter();
		Map<String,Object> map = new HashMap<String,Object>();
    	map.put("resultCode", 1300);
    	map.put("resultMsg", "系统处理中...请不要频繁提交！");        	
    	logger.debug("系统处理中...请不要频繁提交！");
    	out.write(JSON.toJSONString(map));
		out.close();
		return false;  
	}

	public TokenValidator getTokenValidator() {
		return tokenValidator;
	}

	public void setTokenValidator(TokenValidator tokenValidator) {
		this.tokenValidator = tokenValidator;
	}
	
}

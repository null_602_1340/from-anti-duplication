package com.yun.main.interceptor;

import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

/**
 * 
 * <p>token的仓库 </p>
 * @Package com.yun.main.interceptor 
 * @author 黄乡南
 * @e-mail 823798218@qq.com
 * @date 2016-7-1 下午2:10:30 
 * @version V1.0
 */
public class RedisTokenStorage implements TokenStorage{
	
	protected StringRedisTemplate redisTemplate;
	

	public RedisTokenStorage(StringRedisTemplate redisTemplate) {
		super();
		this.redisTemplate = redisTemplate;
	}



	public boolean save(FormToken formToken){
		if(formToken==null){
			throw new RuntimeException("参数formToken不能未null");
		}
		ValueOperations<String, String> vo = redisTemplate.opsForValue();
		vo.set(formToken.getTokenKey(), formToken.getTokenVal(), formToken.getExpires(), TimeUnit.MILLISECONDS);
		return true;
	}
	


	@Override
	public String get(String formTokenKey) {
		return redisTemplate.opsForValue().get(formTokenKey);
	}


	@Override
	public void delete(String formTokenKey) {
		if (isNotBlank(redisTemplate.opsForValue().get(
				formTokenKey))) {
			redisTemplate.opsForValue().set(formTokenKey, "");
		}
	}
	  private boolean isNotBlank(String string) {
		return !isBlank(string);
	}



	public static boolean isBlank(final CharSequence cs) {
	        int strLen;
	        if (cs == null || (strLen = cs.length()) == 0) {
	            return true;
	        }
	        for (int i = 0; i < strLen; i++) {
	            if (Character.isWhitespace(cs.charAt(i)) == false) {
	                return false;
	            }
	        }
	        return true;
	    }

}

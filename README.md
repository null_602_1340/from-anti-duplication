#from-anti-duplication
## 项目介绍
from-anti-duplication是用于java web应用服务器端提供表单防重复提交拦截功能的框架。适用于以下三种场景：
1. 场景一：在网络延迟的情况下让用户有时间点击多次submit按钮导致表单重复提交
2. 场景二：表单提交后用户点击【刷新】按钮导致表单重复提交
3. 场景三：用户提交表单后，点击浏览器的【后退】按钮回退到表单页面后进行再次提交

## 项目结构
结构图：
![输入图片说明](http://git.oschina.net/uploads/images/2016/1116/105159_94dfb9d7_1108227.jpeg "在这里输入图片标题")
时序图
![输入图片说明](http://git.oschina.net/uploads/images/2016/1116/104544_fb22f53f_1108227.jpeg "在这里输入图片标题")
#from-anti-duplication使用帮助
项目默认依赖springmvc，redis，jsp，freemark
maven配置：

```
<dependency>
    <groupId>com.yun</groupId>
    <artifactId>from-anti-duplication-def-impl</artifactId>
    <version>1.0.0</version>		    
</dependency>
```
applicationContext.xml的配置token存储器使用的redis连接池：
```
<!-- redis连接池配置start -->
	<bean id="poolConfig" class="redis.clients.jedis.JedisPoolConfig">
		<property name="maxIdle" value="${redis.maxIdle}" />
		<!-- <property name="maxActive" value="${redis.maxActive}" /> -->

		<!-- <property name="maxWait" value="${redis.maxWait}" /> -->
		<property name="testOnBorrow" value="${redis.testOnBorrow}" />
	</bean>

	<bean id="connectionFactory"
		class="org.springframework.data.redis.connection.jedis.JedisConnectionFactory"
		p:host-name="${redis.host}" p:port="${redis.port}" p:password="${redis.pass}"
		p:pool-config-ref="poolConfig" />

	<bean id="redisTemplate" class="org.springframework.data.redis.core.StringRedisTemplate">
		<property name="connectionFactory" ref="connectionFactory" />

	</bean>
	<!-- redis连接池配置end -->
```
spring-from.xml配置token验证器和存储器：
```
<!-- 使用redis token存储器 -->
	<bean id="tokenStorage" class="com.yun.main.interceptor.RedisTokenStorage">
		<constructor-arg index="0" ref="redisTemplate"></constructor-arg>
	</bean>
	<!-- token验证器，用于验证token和产生token -->
	<bean id="tokenValidator" class="com.yun.main.interceptor.TokenHelper">
		<constructor-arg index="0" ref="tokenStorage"></constructor-arg>
	</bean>
```
spring-mvc.xml配置token拦截器：
```
<mvc:interceptors>
		<!-- 表单重复提交 -->
		<mvc:interceptor>
				 <mvc:mapping path="/**"/> 
					<!-- 设置不拦截的url -->
					<mvc:exclude-mapping path="/view/**" />
					<mvc:exclude-mapping path="/captcha/**" />
					<mvc:exclude-mapping path="/index.html" />
					<mvc:exclude-mapping path="/css/**" />
					<mvc:exclude-mapping path="/js/**" />
					<mvc:exclude-mapping path="/images/**" />				
					<mvc:exclude-mapping path="/common/**" />
					<mvc:exclude-mapping path="/upload/**" />
					<mvc:exclude-mapping path="/favicon.ico"/>
					<mvc:exclude-mapping path="/test.jsp"/>
					<mvc:exclude-mapping path="/404.jsp"/>
					<mvc:exclude-mapping path="/400.jsp"/>
					<mvc:exclude-mapping path="/500.jsp"/>
				<bean class="com.yun.main.interceptor.TokenInterceptor">
				<constructor-arg index="0" ref="tokenValidator"></constructor-arg>
					<!-- 配置多个拦截器 -->
				</bean>
		</mvc:interceptor>
	</mvc:interceptors>
```
如果使用了freemark生成页面，则配置以下自定义freemark标签，用于生成表单的token隐藏域，
在spring-mvc.xml中添加freemark的token标签生成器配置：
```
 <!-- 配置freemarker自定义标签 解析类 -->
	<bean name ="tokenTagFreemarkerDirective" class="com.yun.main.tag.core.TokenTagFreemarkerDirective">
		<constructor-arg index="0" ref="tokenHelper"/>
	</bean>
    <bean id="freeMarkerConfigurer" class="org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer" >
        <property name="templateLoaderPath" value="/WEB-INF/views/" />
        <property name="defaultEncoding" value="UTF-8" />
        <property name="freemarkerSettings">
			<props>
				<prop key="template_update_delay">0</prop>
				<prop key="defaultEncoding">UTF-8</prop>
				<prop key="url_escaping_charset">UTF-8</prop>
				<prop key="locale">zh_CN</prop>
				<prop key="boolean_format">true,false</prop>
				<prop key="datetime_format">yyyy-MM-dd HH:mm:ss</prop>
				<prop key="date_format">yyyy-MM-dd</prop>
				<prop key="time_format">HH:mm:ss</prop>
				<prop key="number_format">0.######</prop>
				<prop key="whitespace_stripping">true</prop>
				<prop key="auto_import">/ftl/pony/index.ftl as p,/ftl/spring.ftl as s</prop>
				<prop key="classic_compatible">true</prop>
				<!-- <prop key="template_exception_handler">com.dzq.core.exception.FreemarkerExceptionHandler</prop> -->
			</props>
		</property>
		<!-- 配置freemarker自定义标签 -->
		<property name="freemarkerVariables">
			<map>
				<entry key="tokentag" value-ref="tokenTagFreemarkerDirective"></entry>
			</map>
		</property>
	</bean>
```
在freemark的模版文件中使用<@tokentag />生成表单隐藏域
fromtest.html:
```
<form action="/formtest/sub-from" method="post">
    <@tokentag />
					
	<p>
	<label>用户名：</label> <input type="text" name="j_username" size="20" class="login_input" />
	</p>
	<p>
        <label>密码：</label>
        <input type="password" name="j_password" size="20" class="login_input" />
        </p>
					<div class="login_bar">
						<input class="sub" type="submit" value="登录" />
					</div>
</form>
```


如果使用jsp生成表单页面则配置以下自定义jsp标签，用于生成表单隐藏域
在WEB-INF/tld文件夹下添加form-tags.tld文件：
```
<?xml version="1.0" encoding="UTF-8"?>
<taglib xmlns="http://java.sun.com/xml/ns/javaee" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://java.sun.com/xml/ns/javaee http://java.sun.com/xml/ns/javaee/web-jsptaglibrary_2_1.xsd"
	version="2.1">

	<description>JSTL 1.1 core library</description>
	<display-name>JSTL core</display-name>
	<tlib-version>1.1</tlib-version>
	<short-name>form</short-name>
	<uri>/form-tags</uri>

	<tag>
		<name>tokentag</name>
		<tag-class>com.yun.main.tag.core.TokenTag</tag-class>
		<body-content>JSP</body-content>
	</tag>
</taglib>
```
在jsp页面添加如下指令来使用from-tokenTag标签：
```
<%@ taglib prefix="form" uri="/form-tags"%>
<form action="/formtest/sub-from" method="post">
    <form:tokentag/>
					<p style="text-align: center; color: red;">${sessionScope['SPRING_SECURITY_LAST_EXCEPTION'].message}</p>
					<p>
						<label>用户名：</label> <input type="text" name="j_username" size="20"
							class="login_input" />
					</p>
					<p>
						<label>密码：</label> <input type="password" name="j_password"
							size="20" class="login_input" />
					</p>
					<div class="login_bar">
						<input class="sub" type="submit" value="登录" />
					</div>
</form>

```
webapp根目录结构:
```
webapp/
    ┗ common/
     ┗ js/
        ┗ comm/
            formtool.js
            jquery.form.js
       jquery-1.11.3.js
    ┗ WEB-INF/
        ┗ jsp/
            formtest-jsp.jsp
        ┗ views/
            formtest.html
```
当使用ajax提交表单时要做token域的更新操作请使用formtool.js提供的ajaxform表单提交功能。该插件基于jquery-1.11.3.js，jquery.form.js开发：
在页面引入jquery-1.11.3.js，jquery.form.js，formtool.js三个js文件。
提交表单示例：
```
<script type='text/javascript' src='${base}/js/jquery-1.11.3.js'></script>

<script type="text/javascript" src="${base}/js/comm/jquery.form.js" ></script>
<script type='text/javascript' src='${base}/js/comm/formtool.js'></script>
<script type="text/javascript">
	function subAjax() {
		$("#ajaxForm").commonAjaxSubmit({
			"success" : function(data) {
				alert(data.resultMsg);
			},
			"error" : function() {
				alert("error");
			}
		});
	}
</script>
<form action="/formtest/ajax-sub-from" method="post" id="ajaxForm">
    <form:tokentag/>
	<p>
	<label>用户名：</label> <input type="text" name="j_username" size="20" class="login_input" />
	</p>
	<p>
	<label>密码：</label> <input type="password" name="j_password"
							size="20" class="login_input" />
	</p>
	<div class="login_bar">
	<input class="sub" type="button" onclick="subAjax()" value="ajax登录" />
	</div>
</form>
```
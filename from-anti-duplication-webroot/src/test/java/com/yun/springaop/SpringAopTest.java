package com.yun.springaop;

import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 
 * 
 * @author 黄乡南
 * @e-mail 823798218@qq.com 
 * @version v1.0
 * @copyright 2010-2015
 * @create-time 2014-6-11 下午8:17:07
 * 
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath*:/applicationContext.xml")
public class SpringAopTest {
	
}

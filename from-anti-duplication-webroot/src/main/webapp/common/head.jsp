<%@page import="org.apache.commons.httpclient.HttpClient"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%--仅用于html的head（公共css、js、meta） --%>
<%@include file="/common/meta.jsp"%>
<%@include file="/common/taglibs.jsp"%>
<link href="${jcPath }/themes/pager.css" type="text/css" rel="stylesheet"/>
<script type="text/javascript" src="${jcPath}/javascript/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${jcPath}/javascript/formtool.js"></script>
